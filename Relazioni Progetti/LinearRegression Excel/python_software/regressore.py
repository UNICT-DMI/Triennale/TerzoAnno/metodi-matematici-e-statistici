# -*- coding: iso-8859-15 -*-

import matplotlib.pyplot as plt
import numpy as np
import xlrd
import os
from sklearn import linear_model
from scipy import stats as st

dataset_dir = 'exelfile'
xlslist = sorted(os.listdir(dataset_dir))

plt.figure()
n = 1

for xls in xlslist:
	xl_workbook = xlrd.open_workbook(dataset_dir +"/"+ xls, encoding_override="ascii")
	xl_sheet = xl_workbook.sheet_by_index(0)

	X = np.array([])
	Y = np.array([])

	for row_idx in range(1, xl_sheet.nrows):
		X = np.append(X, xl_sheet.cell(row_idx, 0).value)
		Y = np.append(Y, xl_sheet.cell(row_idx, 1).value)

	array_X = X
	array_Y = Y
	X = X.reshape(-1,1)
	Y = Y.reshape(-1,1)

	regr = linear_model.LinearRegression()

	regr.fit(X, Y)
	m = regr.coef_
	q = regr.intercept_ 
	covarianza= np.cov(array_X, array_Y)
	coefPears = st.pearsonr(array_X, array_Y)
	plt.subplot(3, 4, n)
	plt.plot(X, Y, 'bx')
	#plt.title("xls: {0} ".format(xls))
	plt.title("xls {4}\n m {0} - q {1}\nCoeff_Pearson {2}\n Covarianza {3}".format(m, q, coefPears[0], covarianza[0,1], xls))	
	plt.plot(X, regr.predict(X), color='red', linewidth=3)

	n+=1

plt.tight_layout(h_pad=0.2, w_pad=0.5)
plt.show()